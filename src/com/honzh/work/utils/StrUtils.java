package com.honzh.work.utils;

import java.util.UUID;

public class StrUtils {

	/**
	 * 生成唯一UUID
	 * 
	 * @Title: uuid
	 * @author Realfighter
	 * @return String
	 * @throws
	 */
	public static String uuid() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
