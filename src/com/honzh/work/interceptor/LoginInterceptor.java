package com.honzh.work.interceptor;

import com.honzh.work.model.User;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * LoginInterceptor.
 */
public class LoginInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		User user = ai.getController().getSessionAttr("honzh_user");
		if (user == null) {
			ai.getController().redirect("/");
		} else {
			ai.invoke();
		}
	}

}
