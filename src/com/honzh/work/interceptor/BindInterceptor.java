package com.honzh.work.interceptor;

import com.honzh.work.model.User;
import com.honzh.work.model.UserMail;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * BindInterceptor 邮箱绑定拦截
 */
public class BindInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		User user = ai.getController().getSessionAttr("honzh_user");
		// 根据用户ID获取邮箱是否绑定
		UserMail userMail = UserMail.dao.findFirst("select * from sys_usermail where userid=?", user.getStr("userid"));
		if (userMail == null) {
			ai.getController().redirect("/toBindEmail.honzh");
		} else {
			ai.invoke();
		}
	}

}
