===========================
基于JFinal + Jquery Mobile开发的workapp
===========================
- 本程序是基于JFinal +Jquery Mobile开发的移动webapp，主要是用于每日工作情况的记录及每日工作邮件的发送，这里要特别感谢二师兄提供的帮助。
- 建议通过手机访问：[http://honzh.xx566.com](http://honzh.xx566.com/)，PC访问也可（效果不佳）。 
- 线上运行效果参见：[http://www.xx566.com/detail/207.html](http://www.xx566.com/detail/207.html)

## 1、应用产生的背景
    工作中需要记录每天的工作情况，每天发送工作邮件，作为考核的依据。二师兄使用ASP.NET开发了PC端工具，试用了一下，还是觉得有些麻烦，想要开发一个手机写日志并发送邮件的移动站。
    
## 2、使用的技术
- JFinal + mysql + jsp
- SpringPlugin
- Jquery Mobile + HTML5
- Jquery datebox 日历插件

## 3、使用说明
1. 下载源码后，需要建立相应的数据库，导入WEB_ROOT/sql下面的sql文件
2. 需要手动的在mst_weekend表中添加本月的休息日，参见数据库注释
3. 修改config.property数据库配置，启动项目即可，通过http://ip:port/workapp访问
    
## 4、主要功能
1. 用户的注册、登录
2. 工作日志管理
    1. 新增工作日志
    2. 发送工作日志
3. 周总结管理
    1. 新增周总结
    2. 发送周总结
4. 邮件发送记录查询
5. 绑定企业邮箱

## 5、注意事项
1. 目前只支持绑定QQ的邮箱产品
2. 请先增加周计划
3. 管理员需要手动的添加休息日

## 6、其它说明
- 时间仓促，代码粗糙，难免存在各种bug，我会陆续完善，如果您的使用发现bug或不便，欢迎提交issue或留言。
- 留言直达：[http://www.xx566.com/message/n2.html](http://www.xx566.com/message/n2.html)  

## 7、源码下载
- [http://git.oschina.net/realfighter/workapp](http://git.oschina.net/realfighter/workapp)
