<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>Welcome，${honzh_user.realname}！</h1>
            <div data-role="navbar">
		      <ul>
		        <li><a href="${ctx}/main.honzh" data-icon="home">工作日志</a></li>
		        <li><a href="${ctx}/mainweek.honzh" data-icon="grid">周总结</a></li>
		         <li><a href="${ctx}/mails.honzh" data-icon="arrow-r" class="ui-btn-active">邮件记录</a></li>
		      </ul>
		    </div>
	    </div>
	    <div data-role="content">
	    	<h3>${date}，今天是<c:if test="${weekend}">休息日</c:if><c:if test="${!weekend}">工作日</c:if>。</h3>
	    	<div data-role="controlgroup" data-type="horizontal">
				<a href="${ctx}/toNote.honzh" data-role="button" data-icon="plus">日志</a>
				<a href="${ctx}/toWeekend.honzh" data-role="button" data-icon="plus">周总结</a>
				<a href="${ctx}/toBindEmail.honzh" data-role="button" data-icon="plus">企业邮箱</a>
			</div>
		     <c:forEach var="mail" items="${page.list}">
		     <form>
		     	<fieldset data-role="collapsible">
		          <legend>
		          【<c:if test="${mail.state==1}">成功</c:if><c:if test="${mail.state==0}">发送中</c:if><c:if test="${mail.state==-1}"><font color="red">失败</font></c:if>】${mail.title}
		         </legend>
		          <label for="sender">标题：${mail.title}</label>
		          <label for="sender">发件人：${mail.sender}</label>
		          <label for="receiver">收件人：${mail.receiver}</label>
		          <label for="createtime">发送时间：${mail.createtime}</label>
		          <label for="state">状态：<c:if test="${mail.state==1}">成功</c:if><c:if test="${mail.state==0}">发送中</c:if><c:if test="${mail.state==-1}"><font color="red">失败</font></c:if></label>
		       </fieldset>
		     </form>
	        </c:forEach>
		    <div data-role="controlgroup" data-type="horizontal" align="center">
		    	<c:if test="${!empty page.list}">
		    		<c:if test="${page.num!=page.first&&page.num!=0}">
				    	<a href="${ctx}/mails.honzh" data-role="button">首页</a>
						<a href="${ctx}/mails/${page.num-1}.honzh" data-role="button">上一页</a>
					</c:if>
					<c:forEach var="i" begin="${page.begin}" end="${page.end}">
						<a href="${ctx}/mails/${i}.honzh" data-role="button" <c:if test="${page.num==i}">class="ui-btn-active"</c:if>>${i}</a>
					</c:forEach>
					<c:if test="${page.navCount!=0&&page.num!=page.last}">
						<a href="${ctx}/mails/${page.next}.honzh" data-role="button">下一页</a>
						<a href="${ctx}/mails/${page.last}.honzh" data-role="button">尾页</a>
					</c:if>
		    	</c:if>
			    <c:if test="${empty page.list}">暂无记录</c:if>
		    </div>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>