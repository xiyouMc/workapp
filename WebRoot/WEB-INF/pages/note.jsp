<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
    <style type="text/css">
    	#login .n-invalid {border: 1px solid #f00;}
    </style>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>新增日志【${month}】</h1>
	    </div>
	    <div data-role="content">
	    	<h3>注意：日期时间不可重复，系统会自动覆盖！</h3>
	        <form method="post" action="${ctx}/note.honzh">
	          	周计划：
	          <c:if test="${empty noteWeeks}">
	          	<a href="toWeekend.honzh" data-role="button">立即添加周计划</a>
	          </c:if>
	          <c:if test="${!empty noteWeeks}">
	          	<select name="note.weekid" id="weekid" data-role="none">
	          </c:if>
	          <c:forEach var="s" items="${noteWeeks}">
	          	 <option value="${s.weekid}">${s.month}第${s.week}周</option>
	          </c:forEach>
		      <c:if test="${!empty noteWeeks}">
	          	</select>
	          </c:if>
	          <label for="date">选择日期：</label>
	          <input name="note.date" id="notedate" data-role="datebox" data-options='{"mode": "datebox","maxDays":${max},"minDays":${min}}' value="" readonly="readonly" data-rule="required;"/>
	          <label for="daywork">当天具体事项：</label>
	          <textarea name="note.daywork"></textarea>
	          <label for="problem">存在问题：</label>
	          <textarea name="note.problem"></textarea>
	          <label for="method">解决措施：</label>
	          <textarea name="note.method"></textarea>
	          <label for="otherwork">其他工作/特别事件：</label>
	          <textarea name="note.otherwork"></textarea>
	           <div data-role="controlgroup" data-type="horizontal">
		          <input type="submit" data-inline="true" value="提交" <c:if test="${empty noteWeeks}">disabled="disabled"</c:if>>
				  <a href="${ctx}/main.honzh" data-role="button">返回主页</a>
		          <c:if test="${empty noteWeeks}">请先添加周计划</c:if>
			  </div>
		    </form>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>