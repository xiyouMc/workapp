<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>Welcome，${honzh_user.realname}！</h1>
            <div data-role="navbar">
		      <ul>
		        <li><a href="${ctx}/main.honzh" data-icon="home">工作日志</a></li>
		        <li><a href="${ctx}/mainweek.honzh" data-icon="grid" class="ui-btn-active">周总结</a></li>
		        <li><a href="${ctx}/mails.honzh" data-icon="arrow-r">邮件记录</a></li>
		      </ul>
		    </div>
	    </div>
	    <div data-role="content">
	    	<h3>${date}，今天是<c:if test="${weekend}">休息日</c:if><c:if test="${!weekend}">工作日</c:if>。</h3>
	    	<div data-role="controlgroup" data-type="horizontal">
				<a href="${ctx}/toNote.honzh" data-role="button" data-icon="plus">日志</a>
				<a href="${ctx}/toWeekend.honzh" data-role="button" data-icon="plus">周总结</a>
				<a href="${ctx}/toBindEmail.honzh" data-role="button" data-icon="plus">企业邮箱</a>
			</div>
			<c:forEach var="noteweek" items="${page.list}">
				<form method="post" action="${ctx}/updateNoteWeek.honzh">
					<input type="hidden" name="noteweek.id" value="${noteweek.id}"/>
			       <fieldset data-role="collapsible">
			          <legend>${noteweek.month}第${noteweek.week}周</legend>
			          <label for="weekplan">周计划：</label>
			          <textarea name="noteweek.weekplan">${noteweek.weekplan}</textarea>
			          <label for="weekplanexecution">周计划执行情况：</label>
			          <textarea name="noteweek.weekplanexecution">${noteweek.weekplanexecution}</textarea>
			          <label for="problem">存在问题：</label>
			          <textarea name="noteweek.problem">${noteweek.problem}</textarea>
			          <label for="method">解决措施：</label>
			          <textarea name="noteweek.method">${noteweek.method}</textarea>
			          <label for="idea">本周收获/新想法：</label>
			          <textarea name="noteweek.idea">${noteweek.idea}</textarea>
			          <div data-role="controlgroup" data-type="horizontal">
				          <input type="submit" data-inline="true" value="提交">
						  <a href="${ctx}/sendWeekEmail/${noteweek.weekid}.honzh" data-role="button">发送周报</a>
					  </div>
			       </fieldset>
			    </form>
		    </c:forEach>
		    <div data-role="controlgroup" data-type="horizontal" align="center">
		    	<c:if test="${!empty page.list}">
		    		<c:if test="${page.num!=page.first&&page.num!=0}">
				    	<a href="${ctx}/mainweek.honzh" data-role="button">首页</a>
						<a href="${ctx}/mainweek/${page.num-1}.honzh" data-role="button">上一页</a>
					</c:if>
					<c:forEach var="i" begin="${page.begin}" end="${page.end}">
						<a href="${ctx}/mainweek/${i}.honzh" data-role="button" <c:if test="${page.num==i}">class="ui-btn-active"</c:if>>${i}</a>
					</c:forEach>
					<c:if test="${page.navCount!=0&&page.num!=page.last}">
						<a href="${ctx}/mainweek/${page.next}.honzh" data-role="button">下一页</a>
						<a href="${ctx}/mainweek/${page.last}.honzh" data-role="button">尾页</a>
					</c:if>
		    	</c:if>
			    <c:if test="${empty page.list}">暂无记录</c:if>
		    </div>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>