<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
    <style type="text/css">
    	#login .n-invalid {border: 1px solid #f00;}
    </style>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>绑定企业邮箱</h1>
	    </div>
	    <div data-role="content">
	    	<h3>注意：多次绑定，系统会自动覆盖！</h3>
	        <form method="post" action="${ctx}/bindEmail.honzh">
	          <label for="email">企业邮箱：</label>
	          <input name="usermail.email"  value="${userMail.email}" data-rule="required;email"/>
	          <label for="password">邮箱密码：</label>
	          <input name="usermail.password" type="password"  value="${userMail.password}" data-rule="required;"/>
	          <label for="receiver">收件人：</label>
	          <input name="usermail.receiver"  value="${userMail.receiver}" data-rule="required;" placeholder="多个请以英文逗号分隔"/>
	          <div data-role="controlgroup" data-type="horizontal">
		          <input type="submit" data-inline="true" value="提交">
				  <a href="${ctx}/main.honzh" data-role="button">返回主页</a>
			  </div>
		    </form>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>