<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>Welcome，${honzh_user.realname}！</h1>
            <div data-role="navbar">
		      <ul>
		        <li><a href="${ctx}/main.honzh" data-icon="home" class="ui-btn-active">工作日志</a></li>
		        <li><a href="${ctx}/mainweek.honzh" data-icon="grid">周总结</a></li>
		        <li><a href="${ctx}/mails.honzh" data-icon="arrow-r">邮件记录</a></li>
		      </ul>
		    </div>
	    </div>
	    <div data-role="content">
	    	<h3>${date}，今天是<c:if test="${weekend}">休息日</c:if><c:if test="${!weekend}">工作日</c:if>。</h3>
	    	<div data-role="controlgroup" data-type="horizontal">
				<a href="${ctx}/toNote.honzh" data-role="button" data-icon="plus">日志</a>
				<a href="${ctx}/toWeekend.honzh" data-role="button" data-icon="plus">周总结</a>
				<a href="${ctx}/toBindEmail.honzh" data-role="button" data-icon="plus">企业邮箱</a>
			</div>
			<c:forEach var="note" items="${page.list}">
				<form method="post" action="${ctx}/updateNote.honzh">
					<input type="hidden" name="note.id" value="${note.id}"/>
			       <fieldset data-role="collapsible">
			          <legend>${note.date}</legend>
			          <label for="daywork">日具体事项：</label>
			          <textarea name="note.daywork">${note.daywork}</textarea>
			          <label for="problem">存在问题：</label>
			          <textarea name="note.problem">${note.problem}</textarea>
			          <label for="method">解决措施：</label>
			          <textarea name="note.method">${note.method}</textarea>
			          <label for="otherwork">其他工作/特别事件：</label>
			          <textarea name="note.otherwork">${note.otherwork}</textarea>
			          <div data-role="controlgroup" data-type="horizontal">
				          <input type="submit" data-inline="true" value="提交">
						  <a href="${ctx}/sendNoteEmail/${note.id}.honzh" data-role="button">发送日志</a>
					  </div>
			       </fieldset>
			    </form>
		    </c:forEach>
		    <div data-role="controlgroup" data-type="horizontal" align="center">
		    	<c:if test="${!empty page.list}">
		    		<c:if test="${page.num!=page.first&&page.num!=0}">
				    	<a href="${ctx}/main.honzh" data-role="button">首页</a>
						<a href="${ctx}/main/${page.num-1}.honzh" data-role="button">上一页</a>
					</c:if>
					<c:forEach var="i" begin="${page.begin}" end="${page.end}">
						<a href="${ctx}/main/${i}.honzh" data-role="button" <c:if test="${page.num==i}">class="ui-btn-active"</c:if>>${i}</a>
					</c:forEach>
					<c:if test="${page.navCount!=0&&page.num!=page.last}">
						<a href="${ctx}/main/${page.next}.honzh" data-role="button">下一页</a>
						<a href="${ctx}/main/${page.last}.honzh" data-role="button">尾页</a>
					</c:if>
		    	</c:if>
			    <c:if test="${empty page.list}">暂无记录</c:if>
		    </div>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>