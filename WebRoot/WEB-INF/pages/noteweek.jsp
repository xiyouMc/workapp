<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
    <style type="text/css">
    	#login .n-invalid {border: 1px solid #f00;}
    </style>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>新增周总结【${month}】</h1>
	    </div>
	    <div data-role="content">
	    	<h3>注意：周期不可重复，系统会自动覆盖！</h3>
	        <form method="post" action="${ctx}/noteweek.honzh">
	          ${month}：
          	  <select name="noteweek.week" data-role="none">
          	  	<option value="1">第一周</option>
          	  	<option value="2">第二周</option>
          	  	<option value="3">第三周</option>
          	  	<option value="4">第四周</option>
          	  	<option value="5">第五周</option>
          	  </select>
	          <label for="weekplan">周计划：</label>
	          <textarea name="noteweek.weekplan"></textarea>
	          <label for="weekplanexecution">周计划执行情况：</label>
	          <textarea name="noteweek.weekplanexecution"></textarea>
	          <label for="problem">存在问题：</label>
	          <textarea name="noteweek.problem"></textarea>
	          <label for="method">解决措施：</label>
	          <textarea name="noteweek.method"></textarea>
	          <label for="idea">本周收获/新想法：</label>
	          <textarea name="noteweek.idea"></textarea>
	           <div data-role="controlgroup" data-type="horizontal">
		          <input type="submit" data-inline="true" value="提交">
				  <a href="${ctx}/mainweek.honzh" data-role="button">返回主页</a>
			  </div>
		    </form>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>