<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <%@include file="/common/common_meta.jsp" %>
</head>
<body>
	<div data-role="page">
	    <div data-role="header">
	        <h1>用户注册</h1>
	    </div>
	    <div data-role="content" align="center">
	    	<c:if test="${result}">
		        <h2>恭喜你,注册成功!</h2>
		        <div data-role="controlgroup">
					<a href="index.honzh" data-role="button">立即登录</a>
				</div>
	    	</c:if>
	    	<c:if test="${!result}">
		        <h2>注册失败,用户名已存在或网络异常！</h2>
		        <div data-role="controlgroup">
					<a href="register.honzh" data-role="button">重新注册</a>
					<a href="index.honzh" data-role="button">返回登录</a>
				</div>
	    	</c:if>
	    </div>
	    <%@include file="/common/common_footer.jsp" %>
	</div>
</body>
</html>