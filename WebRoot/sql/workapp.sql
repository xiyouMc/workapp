/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : workapp

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2015-03-15 21:52:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mst_note
-- ----------------------------
DROP TABLE IF EXISTS `mst_note`;
CREATE TABLE `mst_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userid` varchar(32) NOT NULL COMMENT '用户ID',
  `date` varchar(10) DEFAULT NULL COMMENT '日期，格式2014-01-01',
  `daywork` text COMMENT '今日工作内容',
  `otherwork` text COMMENT '其他工作',
  `problem` text COMMENT '问题阻碍',
  `method` text COMMENT '解决方法',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `weekid` varchar(32) DEFAULT NULL COMMENT '所属周日志ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_date` (`date`,`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_note
-- ----------------------------

-- ----------------------------
-- Table structure for mst_note_week
-- ----------------------------
DROP TABLE IF EXISTS `mst_note_week`;
CREATE TABLE `mst_note_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `weekid` varchar(32) NOT NULL COMMENT '周计划ID',
  `userid` varchar(32) NOT NULL COMMENT '用户ID',
  `week` int(1) DEFAULT NULL COMMENT '与month一起使用，表示本月第几周',
  `weekplan` text COMMENT '周计划',
  `weekplanexecution` text COMMENT '周计划执行情况',
  `idea` text COMMENT '本周收获及新想法',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `month` varchar(20) DEFAULT NULL COMMENT '格式如：2014年01月',
  `problem` text COMMENT '问题阻碍',
  `method` text COMMENT '解决方法',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_note_week
-- ----------------------------

-- ----------------------------
-- Table structure for mst_weekend
-- ----------------------------
DROP TABLE IF EXISTS `mst_weekend`;
CREATE TABLE `mst_weekend` (
  `year` varchar(4) DEFAULT NULL COMMENT '年',
  `month` varchar(2) DEFAULT NULL COMMENT '月',
  `weekend` varchar(50) DEFAULT NULL COMMENT '休息日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mst_weekend
-- ----------------------------
INSERT INTO `mst_weekend` VALUES ('2015', '03', '1#7#8#15#21#22#29');

-- ----------------------------
-- Table structure for sys_mail
-- ----------------------------
DROP TABLE IF EXISTS `sys_mail`;
CREATE TABLE `sys_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `sender` varchar(32) DEFAULT NULL COMMENT '发件邮箱',
  `receiver` varchar(32) DEFAULT NULL COMMENT '收件邮箱',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(50) DEFAULT NULL COMMENT '邮件标题',
  `state` int(1) DEFAULT '0' COMMENT '发送状态，默认为0发送中，1成功，-1失败',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_mail
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `userid` varchar(32) NOT NULL COMMENT '用户GUID',
  `pwd` varchar(32) NOT NULL COMMENT '用户密码',
  `name` varchar(20) NOT NULL COMMENT '用户名',
  `realname` varchar(20) NOT NULL COMMENT '真实姓名',
  `email` varchar(32) NOT NULL COMMENT '邮箱地址',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for sys_usermail
-- ----------------------------
DROP TABLE IF EXISTS `sys_usermail`;
CREATE TABLE `sys_usermail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `email` varchar(32) DEFAULT NULL COMMENT '绑定的企业邮箱',
  `receiver` varchar(200) DEFAULT NULL COMMENT '收件人，逗号分隔',
  `password` varchar(50) DEFAULT NULL COMMENT '企业邮箱密码',
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_usermail
-- ----------------------------
